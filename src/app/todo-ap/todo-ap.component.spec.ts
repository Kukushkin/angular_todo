/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TodoApComponent } from './todo-ap.component';

describe('TodoApComponent', () => {
  let component: TodoApComponent;
  let fixture: ComponentFixture<TodoApComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoApComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoApComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
